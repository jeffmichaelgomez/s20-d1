// //JSON Objects

// {
// 	"city":"Quezon City",
// 	"province":"NCR",
// 	"country":"Philippines"
// }

// //JSON Array

// "cities" [
// 	{"city":"Quezon City","province":"NCR","country":"Philippines"},
// 	{"city":"Caloocan City","province":"NCR","country":"Philippines"},
// 	{"city":"Marikina City","province":"NCR","country":"Philippines"}
// ]

//JSON Methods
let batchesArr = [{ batchName: 'Batch X' },{ batchName: 'Batch Y' }];

//stringyfy convert JS object into a string

console.log('Result from stringify method:');
console.log(JSON.stringify(batchesArr));

let data = {
	name: 'John',
	age: 31,
	address: {
		city: 'Manila',
		country: 'Philippines'
	}
};

console.log(data);
console.log(JSON.stringify(data));

let firstName = prompt(`What is your first name?`);
let lastName = prompt(`What is your last name?`);
let age = prompt(`What is your age?`);
let address = {
	city: prompt(`Which city do you live in?`),
	county: prompt(`Which country does your city address belong to?`)
};


let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
});

console.log(otherData);